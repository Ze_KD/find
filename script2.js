var map = []
var mode = 1
var save = Object();
save.wall = []
var mousedown = false
var IsEnd = false

window.onload = setTimeout(function(){ ChangeSize() },100)

function load(nb=10){
    console.log('load',nb)
    var table = document.createElement('table')
    table.id='table'
    map = []
    for (let i=0 ; i<nb ; i++){
        map[i]=[]
        var tr = document.createElement('tr')
        tr.id = 'tr'+i
        for (let j=0 ; j<nb ; j++){
            map[i][j]=Object()
            map[i][j].type = 0
            map[i][j].dA = -1
            map[i][j].dB = -1
            map[i][j].dS = -1
            map[i][j].p = -1

            var td = document.createElement('td')
            td.id = ''+i+'-'+j
            td.onclick = clic
            tr.appendChild(td)
        }
        table.appendChild(tr)
    }
    document.getElementById('body').appendChild(table)
}

function find(){
    if ( ! (save.x2==undefined || save.y2==undefined || IsEnd )){
        while ( !(x==save.x2 && y==save.y2)){
            findNext(0)
        }
        end()
    }
}

function find2(t=10){
    if ( ! (save.x2==undefined || save.y2==undefined || IsEnd )){
        var loop = setInterval (function(){
            if (findNext(2)=='end'){
                clearInterval(loop)
            }
        },t)
    }
}

function findNext(type){
    if ( ! (save.x2==undefined || save.y2==undefined || IsEnd )){
        console.log('findNext',type)
        min = minD()
        x = min[0]
        y = min[1]
        defD(x,y)
        if (x==save.x2 && y==save.y2){
            end(x,y)
            if (type==2){
                return 'end'
            }
        }
        
    }
}

function end(x,y){
    IsEnd = true
    var xy = map[x][y].p
    x = xy[0]
    y = xy[1]
    while ( !(x==save.x1 && y==save.y1)){
        document.getElementById(''+x+'-'+y).classList.add('ReTrace')
        xy = map[x][y].p
        x = xy[0]
        y = xy[1]
    }
    console.log('end')
}

function defD(x,y){
    //console.log('defD',x,y)
    map[x][y].type=1
    if (document.getElementById(''+x+'-'+y).classList.length == 0){
        document.getElementById(''+x+'-'+y).classList.add('trace')
    }
    var dA ;
    var dB, dS ;
    for (let i=-1 ; i<2 ; i++){
        for (let j = -1 ; j<2 ; j++){
            if (i == 0 || j==0){dA = map[x][y].dA+10}
            else {dA= map[x][y].dA + 14}
            try {
                dB = round(distance(x+i,y+j,save.x2,save.y2)*10)
                dS = dA+dB
                if (
                    map[x+i][y+j].dS > dS ||
                    map[x+i][y+j].dS==-1 ||
                    (map[x+i][y+j].dS == dS && map[x+i][y+j].dB > dB)
                ){
                    map[x+i][y+j].dA = round(dA)
                    map[x+i][y+j].dB = round(dB)
                    map[x+i][y+j].dS = round(dS)
                    map[x+i][y+j].p = [x,y]
                }
            }
            catch (e){
                //console.log('error',x,y,x+i,y+j,'not def')
            }
        }
    }

    show()
}

function minD(){
    //console.log('minD')
    var x , y ;
    var dS = -1
    var dB = -1
    for (let i = 0 ; i<map.length; i++){
        for (let j=0 ; j<map[i].length ; j++){
            if (map[i][j].dS != -1  && map[i][j].type == 0){
                if (
                    map[i][j].dS < dS || 
                    dS == -1 || 
                    (map[i][j].dS <= dS && map[i][j].dB < dB)
                ){
                    //console.log('Change',i,j,x,y)
                    x = i
                    y = j
                    dS = map[i][j].dS
                    dB = map[i][j].dB
                    
                }
                //else { console.log('bad Ds')}
                //console.log('laps',i,j,map[i][j].dS,dB,x,y,dS,dB)
            }
        }
    }
    //console.log('Min D in',x,y,map[x][y].dS)
    return [x,y]
}

function show(v=false){
    //console.log('show')
    for (let i = 0 ; i<map.length; i++){
        for (let j=0 ; j<map[i].length ; j++){
            var txt = ''+round(map[i][j].dS)
            if (v){
                txt = ''+round(map[i][j].dA)+';'+round(map[i][j].dB)+';'+round(map[i][j].dS)
            }
            document.getElementById(''+i+'-'+j).innerHTML  = txt
        }
    }
}



