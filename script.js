function ChangeSize(){
    if (document.getElementById('table') != undefined){
        document.getElementById('table').remove()
    }
    //console.log('select',e.target[e.target.selectedIndex].innerHTML )
    var doc = document.getElementById('select')
    load(doc[doc.selectedIndex].innerHTML)
    mode = 1
    IsEnd = false
}

function mouse(event){
    if (mousedown){
        clic(event,'mouse')
    }
}

function distance(x,y,x1=0,y1=0){
    return ((x-x1)**2+(y-y1)**2)**0.5
}

function round(x,n=0){
    return Number(x.toFixed(n))
}

function clic(n,type=''){
    
    if (n.target.localName == "td"){
        if (mode==1){
            n.target.classList.add('start')
            mode++

            save.x1 = n.target.id.split('-')[0]
            save.y1 = n.target.id.split('-')[1]

            map[save.x1][save.y1].dA = 0
            show()
        }
        else if (mode==2){
            n.target.classList.add('end')
            mode++
            save.x2 = n.target.id.split('-')[0]
            save.y2 = n.target.id.split('-')[1]

            map[save.x1][save.y1].dB = round(distance(save.x1,save.x1,save.x2,save.y2),1)*10
            map[save.x1][save.y1].dS = round(map[save.x1][save.y1].dA + map[save.x1][save.y1].dB,1)
            show()
        }
        else if (n.target.classList.length==0){
            //console.log('t',n,n.target,n.target.localName)
            n.target.classList.add('wall')
            var obj = Object()
            obj.x = n.target.id.split('-')[0]
            obj.y = n.target.id.split('-')[1]
            map[obj.x][obj.y].type=1
        }
        else if (n.target.classList[0]=='wall' && type != 'mouse'){
            n.target.classList.remove('wall')
            console.log(n.target,n.target.id,)
            var obj = Object()
            obj.x = n.target.id.split('-')[0]
            obj.y = n.target.id.split('-')[1]
            map[obj.x][obj.y].type=0
        }
    }
    //else {console.log('t',n,n.target,n.target.localName)}
}

function key(e){
    if (e.key == ' ' || e.key=='Enter'){
        findNext('key')
    }
}